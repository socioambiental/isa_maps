(function ($) {
  'use strict';

  // Initialize all maps
  $.each(drupalSettings.isa_maps.maps, function(map_id) {
    // Check if element exist
    if ($('#' + map_id).length == 0) {
      return;
    }

    // Check if it should be initialized
    if (drupalSettings.isa_maps.maps[map_id].initialize == false) {
      return;
    }

    $(document).on('isaMapLoaded', function() {
      $('#' + map_id).isaMap({
        arps             : drupalSettings.isa_maps.maps[map_id].arp_id != false ? [ drupalSettings.isa_maps.maps[map_id].arp_id ] : [],
        layers           : drupalSettings.isa_maps.maps[map_id].arp_id == false ?   drupalSettings.isa_maps.maps[map_id].layers   : [ 'jurisdiction.amlegal' ],
        expandedGroups   : drupalSettings.isa_maps.maps[map_id].expandedGroups,
        lang             : drupalSettings.isa_maps.maps[map_id].lang,
        //imagesUrl      : drupalSettings.isa_maps.maps[map_id].imagesUrl,
        proxyUrl         : '/isa_services/arcgisproxy/proxy',
        geocodeUrl       : '/isa_services/geocode',
        debug            : true,
        urlChange        : false,
        scrollWheelZoom  : false,
        searchControl    : false,
        embedControl     : false,
        localeControl    : false,
        fileLayerControl : false,
      });
    });
  });
})(jQuery);
