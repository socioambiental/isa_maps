(function ($) {
  'use strict';

  $(document).ready(function() {
    var widget       = $('#tab-pesquisa')[0];
    var map          = 'map_widget';
    var mapId        = '#' + map;
    var arpTypes     = [ 'ti', 'uc' ];
    var arpType      = drupalSettings.isa_maps.maps[map].arp_type + 's';
    var lang         = drupalSettings.isa_maps.maps[map].lang;
    var url          = '/' + lang + '/pesquisa';
    var htmlResults  = { };
    var jsonResults  = { };
    var selectors    = '#filter-ti, #filter-uc, #filter-povo, #filter-grupo, #filter-instancia, #filter-uf, #filter-bacia, #filter-funai, #filter-categoria, #filter-bioma, #filter-municipio';
    var arpSelectors = '#filter-ti, #filter-uc'

    // Fullscreen mechanics
    $('#filter-fullscreen').click(function() {
      if (screenfull.enabled) {
        screenfull.toggle(widget);
      }
    });

    // Reset selects
    function resetSelect(selector) {
      $(selector).val(0);
      $(selector).trigger('chosen:updated');
    }

    // Disable selects
    function disableSelect(selector) {
      resetSelect(selector);
      $(selector).prop('disabled', true).trigger('chosen:updated');
    }

    // Enable selects
    function enableSelect(selector) {
      $(selector).prop('disabled', false).trigger('chosen:updated');
    }

    // Hide previous selection
    function hideCurrent() {
      $(mapId).isaMap('removeArpsById');
      $('#results').html('');
    }

    // Disable clear button
    function disableClearButton() {
      $('#filter-clear').attr('disabled', true);
    }

    // Enable clear button
    function enableClearButton() {
      $('#filter-clear').attr('disabled', false);
    }

    // Show all ARPs of arpType
    function showEverything() {
      if (!$(mapId).isaMap('hasLayer', arpType, 'markers')) {
        if (arpType == 'tis') {
          $(mapId).isaMap('showLayer', arpType, 'markers');
        }
        else if (arpType == 'ucs') {
          $(mapId).isaMap('showLayer', arpType, 'markersFederais');
          $(mapId).isaMap('showLayer', arpType, 'markersEstaduais');
        }

        $('#results').html(htmlResults['default']);
      }
    }

    // Handle clear button click event
    $('#filter-clear').click(function() {
      resetSelect(selectors);
      hideCurrent();
      showEverything();
      enableSelect(selectors);
      disableClearButton();
    });

    // Chosen jQuery widget for select boxes
    $(selectors).chosen({
      'search_contains'         : true,
      'placeholder_text_single' : Drupal.t('Select an Option'),
      'no_results_text'         : Drupal.t('No results match'),
    });

    // Handle select boxes change event
    $(selectors).change(function() {
      var current = { };

      // Build argument list
      $(selectors).each(function() {
        if (this.value == 0) {
          return;
        }

        var param      = $(this).attr('id').replace('filter-', '');
        current[param] = this.value;
      });

      applySelection(current);
    });

    // Apply selection into the widget
    function applySelection(selection = {}) {
      // Hide everything previously shown
      hideCurrent();

      if (Object.keys(selection).length === 0 && selection.constructor === Object) {
        showEverything();
        enableSelect(selectors);
        disableClearButton();

        return;
      }
      else {
        // Ensure all ARPs are hidden
        if ($(mapId).isaMap('hasLayer', arpType, 'markers')) {
          $(mapId).isaMap('hideLayer',  arpType, 'markers');
        }

        enableClearButton();
      }

      // Handle special case of a single ARP selected
      for (var item in arpTypes) {
        var type = arpTypes[item];

        // Show a single ARP
        if (selection[type] != undefined && selection[type] != null) {
          // Disable every other selects
          disableSelect('#filtros select:not(#filter-' + type  + ')');

          // Add layers
          addMapElementsById([ selection[type] ]);

          // Add list
          showList({ 'id': selection[type] });

          return;
        }
      }

      // Disable single ARP selects
      disableSelect(arpSelectors);

      // Add list
      showList(selection);

      // Add layers
      showLayers(selection);
    }

    // Add elements into the map
    function addMapElementsById(items) {
      // ARP Limits
      // Due to performance issues only shown when the selection is small
      if (items.length <= 10) {
        $(mapId).isaMap('addArpsById', { 'arps': items, 'type': 'limits', show: true });
      }

      // ARP Markers
      $(mapId).isaMap('addArpsById', { 'arps': items, 'type': 'markers', show: true });
    }

    // Display an HTML list of ARPs
    function showList(selection) {
      // Determine the cache ID based on the selection
      var cacheId = JSON.stringify(selection);

      // Display results in HTML
      selection.format = 'html';
      if (htmlResults[cacheId] != undefined) {
        // Cached result
        $('#results').html(htmlResults[url]);
        $('#results .tablesorter').tablesorter();
      }
      else {
        $.ajax({
          url      : url,
          dataType : 'html',
          data     : selection,
          success  : function (response) {
            // Save results
            htmlResults[cacheId] = response;

            // Display results
            $('#results').html(response);
            $('#results .tablesorter').tablesorter();
          }
        });
      }
    }

    // Display an ARPs into the map
    function showLayers(selection) {
      // Determine the cache ID based on the selection
      var cacheId = JSON.stringify(selection);

      // Add layers
      if (jsonResults[cacheId] != undefined) {
        // If there are already retrieved data, just display it
        addMapElementsById(jsonResults[cacheId]);
      }
      else {
        // Display results on map
        selection.format = 'json';
        $.ajax({
          url      : url,
          dataType : 'json',
          data     : selection,
          success  : function (response) {
            var arp_ids = [];

            // We use these indexes to keep arrays uniform
            var j = 0;
            var k = 0;

            // Anything to do?
            if (response['#data'] == null) {
              //$(mapId).isaMap('unblockUI', 'filters');
              $(mapId).isaMap('statusMessageReady', 'filters');
              return;
            }

            for (var i = 0; i < response['#data'].length; i++) {
              arp_ids[j++] = response['#data'][i].id;
            }

            // Show areas using GIS
            if ($(arp_ids).length != 0) {
              addMapElementsById(arp_ids);
            }

            // Save current selection
            jsonResults[cacheId] = arp_ids;

            // Unblock the UI
            //$(mapId).isaMap('unblockUI', 'filters');
            $(mapId).isaMap('statusMessageReady', 'filters');
          },
        });
      }
    }

    // Default results
    htmlResults['default'] = $('#results').html();

    // Hide everything by default
    resetSelect(selectors);
  });
})(jQuery);
