<?php
/**
 * @file
 * Contains \Drupal\isa_maps\Controller\MapController.
 */

namespace Drupal\isa_maps\Controller;

use Drupal\isa_utils\Controller\BaseController;

class MapController extends BaseController {
  public function index() {
    $output            = $this->defaultOutput;
    $output['#markup'] = '';

    return $output;
  }
}
