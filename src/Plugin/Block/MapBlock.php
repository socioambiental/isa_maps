<?php
/**
 * @file
 * Contains \Drupal\isa_maps\Plugin\Block\MapBlock.
 */

namespace Drupal\isa_maps\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\isa_utils\Library\Path;
use Drupal\isa_utils\Library\Combos;
use Drupal\isa_utils\Library\Ti;
use Drupal\isa_utils\Library\Uc;

/**
 * Provides a 'Map' block.
 *
 * @Block(
 *   id = "isa_maps_map",
 *   admin_label = @Translation("ISA Map"),
 * )
 */
class MapBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config     = \Drupal::config('isa_maps.config');
    $layers     = $config->get('layers');
    $arp_type   = $config->get('arp_type');
    $lang       = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $routing    = \Drupal\isa_utils\Library\Path::get();
    $ti         = new Ti();
    $uc         = new Uc();
    $ti_grid    = $ti->GridTi();
    $uc_grid    = $uc->GridUc();
    $combos     = new Combos();
    $initialize = true;
    $render     = array(
      '#theme'        => 'isa_maps_map',
      '#tipo'         => 'brasil',
      '#caption'      => t('Search'),
      '#map_id'       => 'map_widget',
      '#class'        => 'map last',
      '#combos'       => $combos->render($arp_type),
      '#arp_type'     => $arp_type,
      '#lang'         => $lang,
      '#attached'     => array(
        'library'     => array(
          'isa_maps/map',
        ),
      ),
      '#cache' => array(
        'contexts' => array('url'),
      ),
    );

    // Use default layers if we do not have a default setting
    if ($layers == NULL) {
      $layer = array('tis.limits', 'ucs.limitsEstaduais', 'ucs.limitsFederais', 'jurisdicao.amlegal');
      $render['#help'] = t('Use the filters to search Indigenous Lands or navigate using the map.');
    }

    // Add grid
    if ($arp_type == 'ti') {
      $render['#grid'] = $ti_grid;
    }
    else if ($arp_type == 'uc') {
      $render['#grid'] = $uc_grid;
      $render['#help'] = t('Use the filters to search Conservation Areas or navigate using the map.');
    }

    // Try to get ARP Id using the new convention, falling back to old TI scheme
    $id = \Drupal\isa_utils\Library\Path::getArpId() != false ? \Drupal\isa_utils\Library\Path::getArpId() : \Drupal\isa_utils\Library\Path::getTiId();

    // Context selection
    if ($id != FALSE) {
      // Ignore config: show only basic layers if there's an ARP to be shown
      // This is done already afterwards in the JS code
      //$layers = array('jurisdiction.amlegal');

      $render['#tipo']    = 'arp';
      $render['#help']    = NULL;
      $render['#combos']  = NULL;
      $render['#caption'] = t('Map');

      // Check if ARP is plotted
      if (isset($ti_grid[$id]) && $ti_grid[$id]['plotagem'] == 'Não') {
        $initialize = FALSE;
      }
      else if (isset($uc_grid[$id]) && $uc_grid[$id]->plotagem == 'Não') {
        $initialize = FALSE;
      }
    }
    else if (isset($routing[1]) && $routing[1] == 'mapa') {
      $render['#caption'] = '';
      $render['#help']    = NULL;
    }

    $render['#initialize'] = $initialize;

    // Images URL
    global $base_url;
    $images_url = $base_url .'/'. drupal_get_path('module', 'isa_maps') .'/js/jquery.isamap/images/';

    // JS settings
    $render['#attached']['drupalSettings']['isa_maps']['maps'][$render['#map_id']] = array(
      'arp_id'         => $id,
      'tipo'           => $render['#tipo'],
      'lang'           => $lang,
      'layers'         => $layers,
      //'expandedGroups' => array('tis'),
      'arp_type'       => $arp_type,
      'imagesUrl'      => $images_url,
      'initialize'     => $initialize,
    );

    return $render;
  }
}
